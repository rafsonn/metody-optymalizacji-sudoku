using JuMP
using GLPK

function sudokuSolver(callback::Function, matrix::Array{Int64}) 
    if size(matrix)[1] != size(matrix)[2]
        return "ERROR"
    else    
    board_size = size(matrix)[1]
    step = Int(sqrt(board_size))
    end_part = board_size - step + 1
    end
    
#     INIT
    sudoku = Model(with_optimizer(GLPK.Optimizer))
    @variable(sudoku, x[i=1:board_size, j=1:board_size, k=1:board_size], Bin)
    
#     CONDITIONS
    for i = 1:board_size, j = 1:board_size  # Each row and each column
    # Sum across all the possible digits
    # One and only one of the digits can be in this cell, 
    # so the sum must be equal to one
    @constraint(sudoku, sum(x[i,j,k] for k=1:board_size) == 1)
    end
    # Only one number in each row & column
    for index = 1:board_size  
        for k = 1:board_size  
            @constraint(sudoku, sum(x[index,j,k] for j=1:board_size) == 1)
            @constraint(sudoku, sum(x[i,index,k] for i=1:board_size) == 1)
        end
    end
    # Checking each part for doubles
    for i = 1:step:end_part, j = 1:step:end_part, k = 1:board_size
    @constraint(sudoku, sum(x[r,c,k] for r=i:i+(step-1), c=j:j+(step-1)) == 1)
    end
    # Setting for filled spaces
    for i = 1:board_size, j = 1:board_size
        if matrix[i,j] != 0
            @constraint(sudoku, x[i,j,matrix[i,j]] == 1)
        end
    end
    
#     SOLVE
    optimize!(sudoku)
    
    x_val = value.(x)
    solution = zeros(Int, board_size, board_size)
    
    for i in 1:board_size, j in 1:board_size, k in 1:board_size
        if x_val[i,j,k] == 1
            solution[i,j] = k
        end
    end
    callback(solution)
end

function display(grid::Array{Int64})
    arr_size = size(grid)[1]
    part = sqrt(arr_size)
    el = arr_size * arr_size
    for row in 1:arr_size 
        i = 0
        for col in row:arr_size:el
            i+=1
            grid[col] > 9 && print(grid[col], " ")
            grid[col] < 10 && print(" ", grid[col], " ")
            i % sqrt(arr_size) == 0 && print(" ")
        end
        println()
        row % sqrt(arr_size) == 0 && println()
    end
end