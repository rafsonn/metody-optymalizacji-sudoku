using StatsBase

function generateSudokuGrid(grid_size::Int64, pct_of_empty_cells::Int64)
    if pct_of_empty_cells > 100 || sqrt(grid_size)*sqrt(grid_size) != grid_size
        return "ERROR"
    end
    
    grid = zeros(Int64,(grid_size, grid_size))
    arr_size = size(grid)[1]
    part = Int(sqrt(arr_size))
    el = arr_size * arr_size
#     fill array with simple solution
    # fill first half
    for i in 1:el        
        grid[i] = i % (arr_size + 1)
    end
    # fill second half
    for i in arr_size + 1:arr_size:el 
        for j in i:arr_size+1:el
            grid[j] = grid[j-1]
        end
    end
    
#     shuffle columns
    grid = shuffle(swap_col, grid)
    
#     shuffle rows
    grid = shuffle(swap_row, grid)
    
#     Setting number of empty cells as 20% of all
    empty = Int(floor(el*(pct_of_empty_cells/100)))
    places = sample(1:el, empty, replace = false)
    
#     Clearing cells
    for i in 1:size(places)[1]
        grid[places[i]] = 0
    end
    return grid
end

function swap_col(grid::Array{Int64}, from::Int64, to::Int64)
    arr_size = size(grid)[1]
    temp = 0
    
    temp = grid[1+(from*arr_size):arr_size+(from*arr_size)]
    grid[1+(from*arr_size):arr_size+(from*arr_size)] = grid[1+(to*arr_size):arr_size+(to*arr_size)]
    grid[1+(to*arr_size):arr_size+(to*arr_size)] = temp
    
    return grid
end

function swap_row(grid::Array{Int64}, from::Int64, to::Int64)
    arr_size = size(grid)[1]
    el = arr_size * arr_size
    temp = 0
    
    temp = grid[1+from:arr_size:el+from]
    grid[1+from:arr_size:el+from] = grid[1+to:arr_size:el+to]
    grid[1+to:arr_size:el+to] = temp
    return grid
end

function shuffle(callback::Function, grid::Array{Int64})
    arr_size = size(grid)[1]
    part = Int(sqrt(arr_size))
    b = 0
    e = part-1
    
    for i in 1:arr_size
        rand_vals = rand(b:e, part)
        for j in 1:size(rand_vals)[1]
            grid = callback(grid, j, rand_vals[j])
        end
        if i % part == 0 && i > 1
            b += part
            e += part
        end
    end
    return grid
end