# Optimization methods - Sudoku Solver

Solving sudoku as Binary Integer Linear Programming problem.

## General info

This project is an implementation of generic function for sudoku solver and simple grid generator write in Julia.

## Technologies

Created with:
- [Julia 1.1.0](https://julialang.org/)
- [JuMP v0.19.0](http://www.juliaopt.org/JuMP.jl/v0.19.0/)
- [GLPK.jl](https://github.com/JuliaOpt/GLPK.jl)
- [Jupyter Notebook](https://jupyter.org/)

## Setup

To run this correctly install JuMP.jl and GLPK Solver package.

```julia
import Pkg
Pkg.add("JuMP")
Pkg.add("GLPK")
Pkg.add("StatsBase")
```

## Usage
##### Solver

```julia
# example sudoku grid
sudoku9x9 = [5 3 0 0 7 0 0 0 0;
             6 0 0 1 9 5 0 0 0;
             0 9 8 0 0 0 0 6 0;
             8 0 0 0 6 0 0 0 3;
             4 0 0 8 0 3 0 0 1;
             7 0 0 0 2 0 0 0 6;
             0 6 0 0 0 0 2 8 0;
             0 0 0 4 1 9 0 0 5;
             0 0 0 0 8 0 0 7 9]

sudokuSolver(ret, sudoku9x9) 
#   returns array
#    9×9 Array{Int64,2}:
#    5  3  4  6  7  8  9  1  2
#    6  7  2  1  9  5  3  4  8
#    1  9  8  3  4  2  5  6  7
#    8  5  9  7  6  1  4  2  3
#    4  2  6  8  5  3  7  9  1
#    7  1  3  9  2  4  8  5  6
#    9  6  1  5  3  7  2  8  4
#    2  8  7  4  1  9  6  3  5
#    3  4  5  2  8  6  1  7  9

sudokuSolver(display, sudoku9x9) 
#   returns visulal representation
#    5  3  4   6  7  8   9  1  2  
#    6  7  2   1  9  5   3  4  8  
#    1  9  8   3  4  2   5  6  7  

#    8  5  9   7  6  1   4  2  3  
#    4  2  6   8  5  3   7  9  1  
#    7  1  3   9  2  4   8  5  6  

#    9  6  1   5  3  7   2  8  4  
#    2  8  7   4  1  9   6  3  5  
#    3  4  5   2  8  6   1  7  9  

```
##### Generator

```julia
generateSudokuGrid(16, 45)
#      grid size --^   ^-- percent of empty cells

generateSudokuGrid(9, 60)
#   returns
#    9×9 Array{Int64,2}:
#    0  0  0  0  0  0  0  8  6
#    0  0  1  0  0  0  6  0  2
#    5  0  8  6  0  0  0  2  0
#    8  1  0  0  0  0  0  5  3
#    0  0  0  0  0  0  0  7  5
#    0  0  0  0  9  0  3  1  8
#    6  8  9  0  0  4  5  3  0
#    0  5  0  4  8  0  2  0  0
#    0  2  0  1  5  0  8  0  0

```



## License
[MIT](https://choosealicense.com/licenses/mit/)

